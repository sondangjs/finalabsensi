<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Absensi</title>

    <link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body class="bg-light">
    <div class="jumbotron jumbotron-fluid">
        <div class="container"> 
            <div class="row"> 
             <div class="col-md-1"></div>
             <div class="col-md-1">
                 <img  src="img/logo_pemkot.png" style="width: 100px; height: 100px;margin-top: -25px;">
             </div>
             <div class="col-md-8">
                <h2 class="text-center" style="font-size: 40px;"> Selamat Datang di Aplikasi Absensi</h2>                  
             </div>
         </div>
        </div>
    </div>

<div class="container mt-5">
    <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">     
            <form action="login_process.php" method="POST">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control" type="text" name="username"  />
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" name="password"  />
                </div>
                <br><input type="submit" class="btn btn-success btn-block" name="login" value="Masuk" />
            </form> 
          
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div> 
</body>
</html>