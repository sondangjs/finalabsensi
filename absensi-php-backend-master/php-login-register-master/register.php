<?php
// Create database connection using config file
include_once("header.php"); 
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
<head>
  <meta charset="UTF-8">
  <title>Absensi Pemkot Medan</title>
</head>
<body class="bg-light">
 
<div class="container mt-5">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 class="text-center">Tambah Pengguna</h2>
            <form action="register_process.php" method="POST">
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input class="form-control" type="text" name="nama" required />
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control" type="text" name="username" required/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" type="email" name="email"  required/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" name="password" required />
                </div>
                  <div class="form-group">
                    <label for="phone">No Handphone</label>
                    <input class="form-control" type="number" name="phone_number" required/>
                </div>
                <input type="submit" class="btn btn-success btn-block" name="register" value="Daftar" />
            </form>            
        </div>    
    </div>
</div>
</body>
<?php include_once('footer.php') ?>
</html>