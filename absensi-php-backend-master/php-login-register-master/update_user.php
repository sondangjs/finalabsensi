<?php
// Create database connection using config file
    include_once("header.php"); 
    $id= $_GET["id"]; 
    $data = mysqli_query($konek,"SELECT * from user where user.id=$id");
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
<head>
  <meta charset="UTF-8">
  <title>Absensi Pemkot Medan</title>
</head>
<body class="bg-light">
 
<div class="container mt-5">
    <div class="row">
        <div class="col-md-3"></div>
        <?php 
            while($user =  mysqli_fetch_array($data)){ ?>
             <div class="col-md-6">
                <h2 class="text-center">Registrasi Pengguna</h2>
                <form action="update_user_process.php?id=<?php echo $user["id"];?>" method="POST">
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input class="form-control" type="text" name="nama"  value="<?php echo $user['nama']?>" />
                    </div>
                    
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" name="username" value="<?php echo $user['username'];?>" />
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="email" name="email" value="<?php echo $user['email'];?>" />
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" value="<?php echo $user['password'];?>" />
                    </div>
                      <div class="form-group">
                        <label for="phone">No Handphone</label>
                        <input class="form-control" type="text" name="phone_number" value="<?php echo $user['phone_number'];?>" >
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="update_user" value="Update" />
                </form>            
            </div>    
            
        <?php }  ?>
    </div>
</div>
</body>
<?php include_once('footer.php') ?>
</html>