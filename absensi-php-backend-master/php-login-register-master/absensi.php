<?php 
  include_once("query/absensi_all.php");
?>
<html> 
<head>

</head>
 
<body>

<h1 class="text-center judul" >History Absensi</h1>
<hr class="garisjudul"></hr>
<div class="container-fluid">
  <div class="col-md-12">
    <table width='80%' class="table table-striped table-bordered">
    <thead id="dark">
    <tr>
      <th width='3%'>No</th>
      <th width='20%'>Nama Pegawai</th>
      <th width='30%'>Lokasi</th> 
      <th width='22%'>Tanggal Absen</th>
      <th width='10%'>Gambar</th>   
    </tr>
  </thead>
   
    <?php  
    $i=0;
    while($absen = mysqli_fetch_array($result)) {         
       $i++;
        echo "<tr>";
        echo "<td>".$i."</td>";
        echo "<td>".$absen['nama']."</td>";
        echo "<td>".$absen['location']."</td>";     
        echo "<td>".$absen['tanggalWaktu']."</td>";
        echo "<td>".$absen['gambar']."</td>";
           
    } 
    ?>
    </table>
  </div>
</div>

</body>
 
</html>