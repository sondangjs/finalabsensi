-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15 Agu 2019 pada 08.13
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpsdm_backup`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_waktu` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `checkout`
--

INSERT INTO `checkout` (`id`, `id_user`, `tanggal_waktu`, `status`) VALUES
(1, 1, '2019-07-14', 'Check Out'),
(2, 1, '2019-07-14', 'Check Out'),
(3, 1, '2019-07-14', 'Check Out'),
(4, 1, '2019-07-26', 'Check Out'),
(5, 1, '2019-07-26', 'Check Out'),
(6, 1, '0000-00-00', 'Check Out'),
(7, 0, '0000-00-00', 'checkout'),
(8, 0, '0000-00-00', 'checkout'),
(9, 29, 'Senin,2019-08-05 14:34:05', 'checkout'),
(10, 29, 'Senin,2019-08-05 14:37:40', 'checkout'),
(11, 29, 'Senin,2019-08-05 20:59:28', 'checkout'),
(12, 0, '1', 'i'),
(13, 1, 's', 's'),
(14, 1, 's', 's'),
(15, 29, 'Senin,2019-08-05 21:51:46', 'checkout'),
(16, 29, 'Senin,2019-08-05 22:09:43', 'checkout'),
(17, 29, 'Senin,2019-08-05 22:42:31', 'checkout'),
(18, 29, 'Senin,2019-08-05 22:55:53', 'checkout'),
(19, 29, 'Selasa,2019-08-06 08:21:41', 'checkout'),
(20, 29, 'Kamis,2019-08-08 11:39:31', 'checkout');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE `gambar` (
  `id_gambar` bigint(100) NOT NULL,
  `gambar` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gambar`
--

INSERT INTO `gambar` (`id_gambar`, `gambar`) VALUES
(1, 'C:/xampp/htdocs/UploadWebService/tempatGambarnya/0.png'),
(2, 'C:/xampp/htdocs/UploadWebService/tempatGambarnya/1.png'),
(3, 'C:/xampp/htdocs/UploadWebService/tempatGambarnya/2.png'),
(4, 'C:/xampp/htdocs/UploadWebService/tempatGambarnya/3.png'),
(5, 'C:/xampp/htdocs/UploadWebService/tempatGambarnya/4.png'),
(6, 'C:/xampp/htdocs/UploadWebService/tempatGambarnya/5.png'),
(7, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/6.png'),
(8, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/7.png'),
(9, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/8.png'),
(10, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/9.png'),
(11, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/10.png'),
(12, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/11.png'),
(13, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/12.png'),
(14, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/13.png'),
(15, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/14.png'),
(16, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/15.png'),
(17, 'C:/xampp/htdocs/absensi-php-backend-master/upload/tempatGambarnya/16.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `location` varchar(225) NOT NULL,
  `tanggalWaktu` varchar(200) NOT NULL,
  `id_gambar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lokasi`
--

INSERT INTO `lokasi` (`id`, `id_user`, `location`, `tanggalWaktu`, `id_gambar`) VALUES
(1, 0, 'silaen', '23', 0),
(2, 0, 'medan', '23juli', 0),
(3, 0, 'medan', '23juli', 0),
(4, 0, 'Capital Building, Jl. Putri Hijau No.1A, Kesawan, West Medan, Medan City, North Sumatra 20111, Indonesia\nWest Medan - 20111\nNorth Sumatra\nIndonesia', '', 0),
(5, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(6, 0, '', '', 0),
(7, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(8, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(9, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(10, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(11, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(12, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(13, 0, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', '', 0),
(14, 1, 'JL. Sisingamangaraja, Km. 9 28, Timbang Deli, Medan Amplas, 20148, Sudirejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20216, Indonesia\nKecamatan Medan Kota - 20216\nSumatera Utara\nIndonesia', 'Selasa,2019-07-30 = 20:11:20', 0),
(15, 1, 'JL. Sisingamangaraja, Km. 9 28, Timbang Deli, Medan Amplas, 20148, Sudirejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20216, Indonesia\nKecamatan Medan Kota - 20216\nSumatera Utara\nIndonesia', 'Selasa,2019-07-30 21:04:36', 0),
(16, 1, ', Jl. Panjang Jl. Komp. Green Garden No.1, RT.14/RW.8, Kedoya Utara, Kec. Kb. Jeruk, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11520, Indonesia\nKecamatan Kebon Jeruk - 11520\nDaerah Khusus Ibukota Jakarta\nIndonesia', 'Senin,2019-08-05 14:26:10', 0),
(17, 1, 's', 's', 1),
(18, 29, 'Jl. Air Bersih No.4, Sudirejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20216, Indonesia\nKecamatan Medan Kota - 20216\nSumatera Utara\nIndonesia', 'Senin,2019-08-05 22:41:48', 1),
(19, 29, 'Jl. Air Bersih No.4, Sudirejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20216, Indonesia\nKecamatan Medan Kota - 20216\nSumatera Utara\nIndonesia', 'Senin,2019-08-05 22:55:44', 1),
(20, 29, 'Jl. Raden Saleh No.2, RT.02, Petisah Tengah, Kec. Medan Petisah, Kota Medan, Sumatera Utara 20231, Indonesia\nKecamatan Medan Petisah - 20231\nSumatera Utara\nIndonesia', 'Selasa,2019-08-06 08:21:37', 1),
(21, 29, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', 'Selasa,2019-08-06 11:09:53', 1),
(22, 29, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', 'Selasa,2019-08-06 14:39:19', 0),
(23, 29, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', 'Selasa,2019-08-06 14:44:09', 14),
(24, 29, 'Jl. Putri Hijau No.1, Kesawan, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236, Indonesia\nKecamatan Medan Barat - 20236\nSumatera Utara\nIndonesia', 'Kamis,2019-08-08 11:39:18', 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  `id_upt` int(11) DEFAULT '0',
  `phone_number` varchar(225) NOT NULL,
  `photo` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `remember_token`, `nama`, `id_role`, `id_upt`, `phone_number`, `photo`) VALUES
(1, '', 'admin@email.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'nTw5YyCwYZNxDDKid04cerNs16stRKWYHaAK1zhq1SG3JUNwv3DEE3SWL8Yh', 'Admin Pusat', 2, 0, '', ''),
(2, '', 'upt_1@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'eEv9bIkrDxQ0ZcrgNAqAm6mL5ZHPInkrE4EktmfVwepbWM26caYAgeHXGzmX', 'BALAI PENDIDIKAN DAN PELATIHAN PENERBANGAN PALEMBANG', 3, 1, '', ''),
(3, '', 'admin_badan@email.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'bNC0FYCt7c0E1UMSMPS0GGikNhs9PsqdU7074lUPV4K5nYHJihrrbd5CYnzz', 'Admin Badan', 1, 0, '', ''),
(4, '', 'upt_2@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'SEKOLAH TINGGI TRANSPORTASI DARAT', 3, 2, '', ''),
(5, '', 'upt_3@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'nzHCn5MvyqRk489X6GzHHjTTKFBWQVJot9tPiAcjnSqmHiISy6exikm3UGUB', 'BALAI PENDIDIKAN DAN PELATIHAN TRANSPORTASI DARAT - MEMPAWAH', 3, 3, '', ''),
(6, '', 'upt_4@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'POLITEKNIK PELAYARAN SURABAYA', 3, 4, '', ''),
(7, '', 'upt_5@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'SUgckjhA2N5a8ZGvQkPqrJsO3iQE9RMdiD8u5FvLvIDcm7MoAptwz3KiywuA', 'BALAI PENDIDIKAN DAN PELATIHAN ILMU PELAYARAN - MAUK TANGERANG', 3, 5, '', ''),
(8, '', 'upt_6@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'pNjSzKa8f5sVFGeUuYmdBeFLZW14PMV1dqrzGhqsglBm492ezSzzVagcqm8t', 'BALAI PENDIDIKAN DAN PELATIHAN PELAYARAN - MINAHASA SELATAN', 3, 6, '', ''),
(9, '', 'upt_7@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'BALAI PENDIDIKAN DAN PELATIHAN PENERBANGAN CURUG', 3, 7, '', ''),
(10, '', 'upt_8@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'SEUVtbdCN0BEE7ByeQAjVQaQnyxBHL7yAfSD6o0r8YMt2H1deRmrrGr9me5U', 'BALAI PENDIDIKAN DAN PELATIHAN PENERBANGAN BANYUWANGI', 3, 8, '', ''),
(11, '', 'upt_9@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'zvClbPIagw4ivhbnVvxAv9B4ybx5kBmgMvxEVBmIh3juHSM3WmWDF92jn6ac', 'BALAI PENDIDIKAN DAN PELATIHAN PENERBANGAN BANYUWANGI', 3, 9, '', ''),
(12, '', 'upt_10@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'AKADEMI TEKNIK KESELAMATAN PENERBANGAN - MAKASSAR', 3, 10, '', ''),
(13, '', 'upt_11@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'AgCsj7V88Kh4ynOZlj6fOenySCREFPTilVeVmxeB5R6Uu3tAI9xemm9xv9Oj', 'SEKOLAH TINGGI ILMU PELAYARAN - JAKARTA', 3, 11, '', ''),
(14, '', 'upt_12@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 12, '', ''),
(15, '', 'upt_13@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 13, '', ''),
(16, '', 'upt_14@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 14, '', ''),
(17, '', 'upt_15@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 15, '', ''),
(18, '', 'upt_16@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'VvfQAVoqMH3SFuDi0UIlFgRVpJ3lIeZbkcjjkBluTFg6ehA6KKGTlmRjiCHW', 'UPT', 3, 16, '', ''),
(19, '', 'upt_17@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 17, '', ''),
(20, '', 'upt_18@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 18, '', ''),
(21, '', 'upt_19@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 19, '', ''),
(22, '', 'upt_20@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', 'wY5LuYaWLmZP6r9E4ooew9CKohDUlZSIRslQN4ldD46NsMCa4Lr3E2LFHyK7', 'UPT', 3, 20, '', ''),
(23, '', 'upt_21@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 21, '', ''),
(24, '', 'upt_22@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 22, '', ''),
(25, '', 'upt_23@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', '', 'UPT', 3, 23, '', ''),
(26, '', 'upt_24@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 24, '', ''),
(27, '', 'upt_25@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 25, '', ''),
(28, '', 'upt_26@gmail.com', '$2y$12$rATIdDwAhRtI.8jgjH2rE.MB/gR1QQCacGUg942DCNxKP1cAYGQaC', NULL, 'UPT', 3, 26, '', ''),
(29, '', 'bp3curug@gmail.com', '$2a$04$bmVFqv5LF19wAsH.9pRn9e44gQcO5vhOpKzeTFCcq7gz1.w7y2ErK', '$2a$04$bmVFqv5LF19wAsH.9pRn9e44gQcO5vhOpKzeTFCcq7gz1.w7y2ErK', 'TestAdmin', 3, 0, '', ''),
(120, '', 'sondangsipayung84@gmail.com', 'sondang', '1', 'sondang', 2, 2, '', ''),
(222, '', 'sondangsipayung84@gmail.com', 'sondang', '21', 'sondang', 1, 1, '', ''),
(1223, '', 's@gmail.com', '$2y$10$mHFf.zZoIdfH9c3oASmS6O/ZAxZO7XlRciLpdV21X6bVwEh9TlOwS', NULL, 's', NULL, 0, '', ''),
(1224, '', 'sipayung@gmail.com', '$2y$10$XN.7Sv8cefOnDVzjRvmSsOZI9TQGtX5E5/YunPzdsAHPmBrWKjlt2', NULL, NULL, NULL, 0, '', ''),
(1225, 'spy', 'spy@gmail.com', '$2y$10$u7A/vBoZUEXD51OdMwFNsefFgKr8R0Tb/hlGucBQFOXn8KaVj0LKu', NULL, 'spy', NULL, 0, '0987', ''),
(1228, 'admin', 'admin@gmail.com', 'admin', NULL, 'admin', NULL, 0, '82160020927', ''),
(1229, 'admin', 'admin@gmail.com', '$2y$10$bE6EzHA4pbIW4hjUInJciuQOrQ615eQtYYl0Mfwy1ywyypMK8KruO', NULL, 'admin', NULL, 0, '82160020927', ''),
(1230, 'sondang', 'sondangsipayung84@gmail.com', '$2y$10$zXSyE5USgqrFuMLUd6rX.uPLZX.q.3uvA8IHxWPOmWBzzIlWaoPZe', NULL, 'sondang jelita', NULL, 0, '82160020927', ''),
(1239, 'sondang', 'sondangsipayung84@gmail.com', '$2y$10$NCtIXkiPn9FADP0mw35B2ulPabXmZNiFbQzQrisnsMcWXbe2OxE/u', NULL, 'sondang jelita', NULL, 0, '082160020927', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id_gambar`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id_gambar` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1240;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
